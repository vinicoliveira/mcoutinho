const express = require("express");
const puppeteer = require("puppeteer");
const cheerio = require("cheerio");
const fs = require("fs");
const cors = require("cors");
const app = express();

// Habilita CORS para todas as origens
app.use(cors());

async function scrapeAndSaveData(url) {
  const browser = await puppeteer.launch({ headless: "new" });
  const page = await browser.newPage();

  await page.goto(url, { waitUntil: "domcontentloaded" });
  await new Promise((r) => setTimeout(r, 3000));

  const content = await page.content();
  const $ = cheerio.load(content);

  const dataElements = $("[data-year][data-value]");
  const data = [];

  dataElements.each((index, element) => {
    let dataYear = parseInt($(element).attr("data-year"));
    let dataValue = parseInt($(element).attr("data-value"));

    data.push({ dataYear, dataValue });
  });

  await browser.close();

  return data;
}

app.get("/scrape-and-save", async (req, res) => {
  const websiteUrl = "http://www.mcoutinhopecas.pt/";

  try {
    const scrapedData = await scrapeAndSaveData(websiteUrl);
    console.log(scrapedData);

    // Salvar os dados em um arquivo JSON local
    fs.writeFileSync("scrapedData.json", JSON.stringify(scrapedData, null, 2));

    res.json({ message: "Dados salvos com sucesso em um arquivo JSON local" });
  } catch (error) {
    console.error("Erro durante o scraping e salvamento de dados:", error);
    res.status(500).json({ error: "Erro interno do servidor" });
  }
});

app.get("/get-scraped-data", (req, res) => {
  try {
    // Ler os dados do arquivo JSON local
    const data = JSON.parse(fs.readFileSync("scrapedData.json", "utf-8"));
    res.json({ data });
  } catch (error) {
    console.error("Erro ao ler os dados do arquivo JSON:", error);
    res.status(500).json({ error: "Erro interno do servidor" });
  }
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Servidor Express em execução na porta ${PORT}`);
});
