# Challenge MCoutinho

## Instalação

### NodeJS

Certifique-se de ter o Node.js instalado. Clone o repositório e execute:

```bash
cd backend-node
npm i
node index.js
```

Estará disponível na porta 3000

### PHP

Certifique-se de ter o PHP, Composer e Laravel instalados. Clone o repositório e execute:

```bash
cd backend-php
composer install
php artisan serve
```

Estará disponível na porta 8000

### FRONTEND

```bash
cd frontend
npm i
npm run dev
```

Estará disponivel na porta 9000

#### Endpoints

1- para fazer o scrape e trazer as informações mais atualizadas do site:
/scrape-and-save

2- para listar os dados:
/get-scraped-data
