<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Storage;

class ScrapeController extends Controller
{
    public function scrapeAndSaveData()
    {
        $url = "http://www.mcoutinhopecas.pt/";
        $client = new Client();
        $crawler = $client->request('GET', $url);
        sleep(3);

        $data = $crawler->filter('[data-year][data-value]')->each(function (Crawler $node, $i) {
            $dataYear = (int) $node->attr('data-year');
            $dataValue = (int) $node->attr('data-value');
            return ['dataYear' => $dataYear, 'dataValue' => $dataValue];
        });

        // Salvar os dados em um arquivo JSON local
        Storage::disk('local')->put('scrapedData.json', json_encode($data, JSON_PRETTY_PRINT));

        return response()->json(['message' => 'Dados salvos com sucesso em um arquivo JSON local']);
    }

    public function getScrapedData()
    {
        try {
            // Ler os dados do arquivo JSON local
            $data = json_decode(Storage::disk('local')->get('scrapedData.json'), true);
            return response()->json(['data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Erro interno do servidor'], 500);
        }
    }
}