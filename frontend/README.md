# Challenge

## Description

Challenge - Project <> Task

## Prerequisites

Make sure you have the following tools installed on your system:

- Node.js
- npm (Node Package Manager)

## Setup

1. Install dependencies: `npm install`
2. execute `npm run dev` for local development or `quasar build -m spa` to build the project. It will be available in the `dist/spa` folder.

## Environment Variables

This project is not using `.env` file. The `base_url` is located in the `boot/axios.js` file
