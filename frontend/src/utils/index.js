const FormatDate = (date, noTime = false) => {
  if (date == null) {
    return "-";
  }
  if (date.indexOf("/") > 0) {
    return date;
  }

  const opcoes = {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",

    timeZone: "America/Sao_Paulo",
  };

  if (noTime) {
    delete opcoes.hour;
    delete opcoes.minute;
  }

  var data = new Date(date);
  return data.toLocaleString("pt-BR", opcoes);
};

export { FormatDate };
