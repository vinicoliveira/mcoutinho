const routes = [
  {
    path: "/",
    component: () => import("pages/Project.vue"),
  },

  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Project.vue"),
  },
];

export default routes;
